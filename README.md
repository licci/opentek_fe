# OpenTek Frontend

A nuxt.js project (check out dev branch)

## Build Setup

``` bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```


## License

This project is licensed under the terms of the MIT license.
